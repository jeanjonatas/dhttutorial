# Tutorial de DHT(_Distributed Hash Table_)


##Projeto 4 de Sistemas Distribuídos 2017/1 

#### Aluno:
* Jean Jônatas de Jesus Borba

---

#### O que é DHT ?   

O DHT é um novo modelo de compartilhamento de arquivos peer-to-peer(P2P) que 
permite o usuário esconder os arquivos que foram baixados.O modelo DHT realiza 
a mesma coisa que as tabelas de hash, mas as tabelas também são distribuídas. 
O resultado é que um arquivo tem uma impressão digital única associada a ele, 
mas nenhum nome de arquivo específico.

Um dos principais propósitos do DHT é derrotar sistemas de rastreamento de 
arquivos. Estes sistemas podem ser usados por donos de propriedades intelectuais
para rastrear quem pode ter baixado legal ou ilegalmente seu 
software ou arquivos.

Os sistemas como [Napster](http://br.napster.com/), 
[Freenet](https://freenetproject.org/) utilizam esse tipo de distribuição p2p 
para aproveitar os recursos de compartilhamento rápido de arquivo, incrível 
banda de transmissão e segurança da  identidade do usuário.

#### Características do DHT
O DHT oferece:
* Autonomia e descentralização, onde os pares participantes formam um sistema,
sem conhecer de fato quem está coordenando.

* Tolerância a falhas,o sistema pode corrigir continuamente juntando, excluindo e
substituindo pares

* Escalabilidade, já que o sistema pode adicionar os hashs de acordo com o 
crescimento permitindo milhões de pares.

Usaremos o [Kadmenlia](http://www.ic.unicamp.br/~bit/ensino/mo809_1s13/papers/P2P/Kademlia-%20A%20Peer-to-Peer%20Information%20System%20Based%20on%20the%20XOR%20Metric%20.pdf)
um peer-to-peer que usa o DHT com um número não simultâneo de hashs.
O webtorrent utiliza o Kadmenlia para implementação do DHT, faremos alguns
exemplos para mostrar o seu uso no bittorrent. Esse exemplo pode ser encontrado
na página do NPM, em https://www.npmjs.com/package/bittorrent-dht.

#### Manual do Usuário   

#### Requisitos para execução
Para executar o servidor é necessária a instalação de algumas ferramentas em seu computador:

1. Node.js para execução do servidor, acesse [Node.js](https://nodejs.org) e saiba mais.   
2. Git para o download dos repositórios, acesse [Git website](https://git-scm.com) e saiba mais.        

#### Execução
Após isso deve-se seguir os seguintes passos:    

Para fazer o download dos arquivos necessários para a execução do servidor:  
    
    git clone https://gitlab.com/jeanjonatas/dhttutorial.git
    
Para baixar os módulos, execute os comandos:

    npm install bittorrent-dht
    npm install magnet-uri
    
E então execute o comando abaixo para ver o hash gerado pelo DHT:

    nodejs dht.js
    
#### Vídeo Aula

Abaixo estão os links das video aulas para entender sobre o DHT:

* Aula 1 :https://youtu.be/v4rDY_aw6UI
* Aula 2 :https://youtu.be/NBcIfr0W_6k
* Aula 3 :https://youtu.be/AF9l175IJ_w

