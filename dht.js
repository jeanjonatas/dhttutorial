'use strict';
var DHT = require('bittorrent-dht');    //Preciso da biblioteca do bittorrent do dht para executar as funções
var magnet = require('magnet-uri');     //Biblioteca do magnet link do bittorrent para expor onde estará o link

//cores console
var red, blue, reset;
red = '\u001b[31m';
blue = '\u001b[34m';
reset = '\u001b[0m';


var uri = 'magnet:?xt=urn:btih:e3811b9539cacff680e418124272177c47477157'; //link magnetico com o hash
var parsed = magnet(uri);

function informarHash(){
console.log('Informação do HASH:');
console.log(red+parsed.infoHash);       //mostra as informações do hash no console
}

informarHash();

var dht = new DHT();    // Criando um dht


dht.listen(8080, function () {
  console.log('Rodando na porta 8080');   
});

dht.on('peer', function (peer, infoHash, from) {
  console.log('Encontrando par ' + peer.host + ':' + peer.port + ' through ' + from.address + ':' + from.port)
})

console.log(reset);